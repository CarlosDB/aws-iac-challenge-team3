<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->indexed();
            $table->text('description');
            $table->nullableTimestamps();
        });

        // Create the book The Hunger Games
        DB::table('books')->insert([
            'name'       => 'The Hunger Games',
            'slug'       => 'TheHungerGames',
            'description'   => 'The Hunger Games are an annual televised event where the ruthless Capitol randomly selects one boy and one girl,    each between the ages of 12 and 18 from each of the twelve districts, pitting them against each other in a game of survival where they are forced to fight one another to the death. The victor then wins a new house for themselves and their families in their district, along with food, fame, and wealth.',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

                // Create the book Lord Of The Rings
        DB::table('books')->insert([
            'name'       => 'Lord Of The Rings',
            'slug'       => 'LordOfTheRings',
            'description'   => "The Lord of the Rings is the saga of a group of sometimes reluctant heroes who set forth to save their world from consummate evil. Its many worlds and creatures were drawn from Tolkien's extensive knowledge of philology and folklore.",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        // Create the book Harry Potter
        DB::table('books')->insert([
            'name'       => 'Harry Potter',
            'slug'       => 'HarryPotter',
            'description'   => 'Harry Potter is a series of seven fantasy novels written by British author J. K. Rowling. The novels chronicle the lives of a young wizard, Harry Potter, and his friends Hermione Granger and Ron Weasley, all of whom are students at Hogwarts School of Witchcraft and Wizardry.',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('books');
    }
}
