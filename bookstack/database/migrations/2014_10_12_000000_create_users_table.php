<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->rememberToken();
            $table->nullableTimestamps();
        });

        // Create the initial admin user
        DB::table('users')->insert([
            'name'       => 'Admin',
            'email'      => 'admin@admin.com',
            'password'   => bcrypt('password'),
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        // Create the user Carlos
        DB::table('users')->insert([
            'name'       => 'Carlos',
            'email'      => 'carlos@bookstack.com',
            'password'   => bcrypt('password'),
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        // Create the user Tim
        DB::table('users')->insert([
            'name'       => 'Tim',
            'email'      => 'tim@bookstack.com',
            'password'   => bcrypt('password'),
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        // Create the user Els
        DB::table('users')->insert([
            'name'       => 'Els',
            'email'      => 'els@bookstack.com',
            'password'   => bcrypt('password'),
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        // Create the user Femke
        DB::table('users')->insert([
            'name'       => 'Femke',
            'email'      => 'femke@bookstack.com',
            'password'   => bcrypt('password'),
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        // Create the user Pjotr
        DB::table('users')->insert([
            'name'       => 'Pjotr',
            'email'      => 'pjotr@bookstack.com',
            'password'   => bcrypt('password'),
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
