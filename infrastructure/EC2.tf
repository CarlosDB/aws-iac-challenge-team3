data "template_file" "startup_script" {
  template = file("${path.module}/install.sh")
  vars = {
    dbname = var.db_username
    dbhost = aws_db_instance.tutorial_database.address
    dbpass = var.db_password
    access_id = var.aws_access_key
    secret_key = var.aws_secret_key
    session_token = var.aws_token
    region = var.aws_region
  }
}

resource "aws_instance" "tutorial_web" {
  count                  = var.settings.web_app.count
  ami                    = "ami-08c40ec9ead489470"
  instance_type          = var.settings.web_app.instance_type
  subnet_id              = aws_subnet.tutorial_public_subnet[count.index].id
  key_name               = "Stack"
  vpc_security_group_ids = [aws_security_group.tutorial_web_sg.id]
  user_data  = data.template_file.startup_script.rendered
  tags = {
    Name = "CodeDeployDemo"
  }
}

resource "aws_eip" "tutorial_web_eip" {
  depends_on = [aws_instance.tutorial_web]
  count    = var.settings.web_app.count
  instance = aws_instance.tutorial_web[count.index].id
  vpc      = true
  tags = {
    Name = "tutorial_web_eip_${count.index}"
  }
}
