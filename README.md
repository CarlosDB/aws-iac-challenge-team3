# Automatisch deployen van BookStack in een AWS EC2 instance- Team 3
## Inleiding
In deze README.md vindt u uitleg over hoe een Bookstackplatform automatisch kan worden opgezet. Er volgt een uiteenzetting over uit welke componenten onze oplossing bestaat, de werking en hoe deze het best gebruikt word. We maken gebruik van de Gitlab CI/CD pipeline om de web applicatie, Bookstack, te deployen. Bootstack is een open-source wiki-applicatie. Deze applicatie beschikt over een web front-end en een backend database. Voor de resources en services op te starten maken we gebruik van AWS. 

## Mappenstructuur 
In onze main branch zijn er 2 mappen te vinden. In de map "bookstack" is alle data van de applicatie te vinden. In de map "infrastructure", bevinden zich de terraform files die worden gebruikt in de pipeline. De pipeline zelf is te vinden buiten de mappenstructuur in de ".gitlab-ci.yml". 

## Front-end
De content van de website bestaat uit verschillende info pagina's, deze zijn doorzoekbaar met de zoekfunctie. Op deze manier is het mogelijk om in het book niveau te zoeken door alle boeken, hoofdstukken en pagina's. Gebruikers kunnen de taal aanpassen naar hun gewenste taal, BookStack beschikt over een groot aantal talen zoals: engels, frans, spaans, nederlands en nog meer. Het is ook mogelijk voor de gebruiker om te kiezen tussen dark en light mode, afhankelijk van hun voorkeur. 

## Back-end
BookStack maakt gebruik van PHP samen met het Laravel framework voor front-end van de applicatie. De pagina-editor is een simpele WYSIWYG (what you see is what you get) en al de content kan in 3 simpele groepen worden ingedeeld: boeken, hoofdstukken en pagina's. In de pagina-editor van BookStack zit ook een diagrams.net tekenmogelijkheid ingebouwd, zodat u snel en gemakkelijk diagrammen kunt maken in uw documentatie. Naast al deze functies kunt u ook in Markdown schrijven als uw voorkeur hiernaar uitgaat. Hiernaast zijn er nog veel meer functies binnen BookStack.

## Terraform
**main.tf**  <br />
In de main.tf file, word getoond met welke versie van AWS er gewerkt word. In dit geval 4.0.0 . Hier zijn ook de variabelen te vinden die verwijzen naar de credentials gelinkt aan de aws account. Deze zijn gedefinieerd in de Gitlab omgeving zelf onder 'Instellingen > CI/CD > Variables'.
We zeggen dat AWS toegang krijgt tot alle beschikbaarheidszones met status beschikbaar. <br />

We maken een Virtual Prive Cloud (VPC) aan. Dit isoleert onze toepassing van de openbare cloud. De ipv4 range (cidr), staat verder gedefinieerd bij de file variables.tf. Voor connectie tussen de VPC en het internet zetten we een internet_gateway. Deze is gelinkt aan de VPC via de vpc_id. <br />

Vervolgens maken we een public subnet aan. Dit is voor de resources die geconnecteerd moeten zijn aan het internet. We maken ook een private subnet aan, dit is voor de resources dat geen internet connectie vragen. Deze word ook aan de VPC gelinkt via de vpc_id. De juiste cidr_block en availabilty zone word toegewezen via de count.index. Deze index is te vinden in 'variables.tf' onder variabele '"subnet_count" > default'.<br />

Daarna maken we ook een "aws_route_table" aan. Dit creëert een resource voor een VPC routing tabel. Deze is nodig voor het verkeer binnen het netwerk te beheren. In de aws_route_table maken we een default route aan. We associëren de routing tabel met het private subnet en het public subnet, zodat hier ook een communicatie tussen ontstaat. <br />

Tenslotte voegen we nog security groups toe. We maken een securitygroep voor de webserver en voor de databases. Voor de webserver laten we al het HTTP trafiek dat via het tcp protocol van poort 80 naar poort 80 gaat door. Verbinding van SSH is enkel beschikbaar van de computer verbonden met de opstelling. We laten al het outbound trafiek toe. Bij de database securitygroep laten we enkel MySQL trafiek toe van en naar poort 3306.

Op het einde van de file word nog een subnetgroep aangemaakt voor de database.

**variables.tf** <br />
In het variabeles.tf bestand worden alle variabelen, die in andere documenten gebruikt worden, gedefinieerd. <br />

De eerste set variabele bevatten  de credentials voor de AWS omgeving. Natuurlijk zetten we deze er niet hardcoded in. Er word een link gelegd naar de gilab variabelen. Vervolgens word de regio van onze aws_regio bepaald. Deze bepaald waar alles bewaard worden. Dit kan impact hebben op de kostprijs van alle componenten die nodig zijn om je omgeving op te zetten. Het heeft ook invloed op de tijdsloop van het aanmaken van je omgeving en het versturen van de data. Het is logisch dat hoe verder je regio is van je huidige locatie, hoe langer het zal duren. <br />
Vervolgens kan u de ipv4 range van onze VPC zien. Daaronder de verschillende subnetten met hun id. Vervolgens worden er nog configuratie instellingen bepaald voor de database en de web app. <br />

Voor de database zijn de volgende gespecificeerd:
- storage in giganytes
- engine type
- engine versie
- rds instantie type
- database naam
 <br />

 Voor de web app zijn de volgende gespecificeerd:
 - aantal EC2 instanties
 - type EC2 instantie
<br />

Tenslotte geven we nog een oplijsting van welke ipv4 ranges mogen gebruikt worden voor het publiek en welke voor het privaatsubnetwerk.

**outputs.tf** <br />
Deze file is verantwoordelijk voor verschillende outputs te tonen zoals:
- de publieke ip van de webserver
- het publiek dns adres
- het adres van het endpoint waar de database terug te vinden is
- de poort via waar de database beschikbaar is


**terraform.tfstate** <br />
Deze file word aangemaakt wanneer een pipeline word gerund. Dit is de cache van infrastructuur. Het is dus belangrijk dat deze file niet word bewerkt. Dit bestand word dan ook verwijderd na de destroy functie te runnen. 

**EC2.tf** <br />
In dit bestand word de installatie van EC2 bepaald. Het beschrijft aan welke vereisten er moet worden voldaan. Hier worden ook linken gelegd naar het variable bestand. <br />
Er word ook een aws_instance aangemaakt, deze laat toe dat de EC2 instance kan worden aangemaakt, bewerkt en verwijderd. Ze ondersteunen ook de provisioning. 
<br />
Ten slotte word er nog een "aws_eip" (Elastic IP resource) aangemaakt. Deze geven we op als een DNS-record zodat het domein naar onze instantie wijst. 


**RDS.tf** <br />
In onze toepassing werken we met MySQL, dit is een relationele database (RDS). Deze is beschikbaar via een "aws_db_instance". De specificaties waar deze database aan moet voldoen worden via variabele opgevraagd uit het 'variables.tf' bestand.

## Pipeline
Onze pipeline is te vinden onder '.gitlab-ci.yml'. Deze pipline zorgt voor een automatische opzetting van de omgeving. Eerst zeggen we dat we gebruik maken van de laatste terraform versie. <br />

Voor het script word uitgevoerd, gaan we naar de map 'infrastructure. We zorgen er voor dat de backend configuratie loopt via het 'GITLAB_TF_ADDRESS', dat bovenaan het document word gedefinieerd. <br />

Er word bepaald dat het cache (teraform.tfstate) bewaart word onder de folder 'infrastructure'. <br />

Vervolgens komt er een oplijsting van de verschillende stages dat onze pipeline gaat doorlopen:
- validate
- plan
- apply
- build
- deploy
- test
- destroy
- notification
<br />

**validate** <br/>
In de stap 'validate' gaan we na of de syntax en de referenties kloppen. Dat alles documenten die nodig zijn, beschikbaar zijn. 
<br />

**Plan** <br />
De volgende face is 'Plan'. Deze kijkt na welke resources er nodig zijn. Het onderzoekt welke resources moeten worden bijgemaakt, gewijzigd en verwijderd.
<br />

**Apply** <br />
De stap dat volgt is 'Apply'. In deze stap word alles wat de plan fase heeft opgelijst, toegepast. De output word bewaard in de 'public.env' file.
<br />

**Build** <br />
Voor de 'build' stap kan worden uitgevoerd moeten we zip toevoegen en curl updaten. Vervolgens zetten we de folder bookstack in een zip bestand zodat deze minder ruimte in beslag neemt en dus sneller kan worden verplaatst.
<br />

**Deploy** <br />
Bij de deploy fase zeggen we eerst van welke variabele we gaan gebruik maken. Vervolgens voegen we 'openssh-client' toe en geven we deze de juiste rechten.<br />

We maken via ssh een connectie met machine van de toepassing. Hier navigeren we naar de '/var/www/html/' directory, waar we de zip met de applicatie info uitladen. Daarna verwijderen de nog bestaande zip file. Na dit gebeurd is, word composer geïnstalleerd. De .env.example word verplaatst naar de .env gekopieerd. Vervolgens word via enkele bestanden via sed -i toegevoegd aan de '.env'. Daarna word een php artisan key gegenereerd en vervolgens word php artisan gemigreerd. We veranderen enkele rechten aan folders zodat we aankunnen waar nodig. De a2enmod word herschreven en de bookstack.conf word verplaats naar '/etc/apache2/sites-available'. We bewerken via a2ensite (scrip dat bepaalde gespecificeerde sites enables) de bookstack.conf.Ten slotte herstarten we Apache2<br />

**Test** <br />
In de testfase word bereikbaarheid van de applicatie nagegaan. Dit gebeurd via een curl-commando.
<br />

**Notification** <br />
In dit deel word er gekenen naar de status. Als het een succes was word de succes_notification gestuurd. Indien deze niet succesvol is word de failure_notification gestuurd. Dit word gestuurd vanuit send.sh.
<br />

**Destroy** <br />
In deze fase worden alle resources verwijderd.

## instal.sh
Bij de Instal.sh installeren we de apache2 service en php 8.1 . We zorgen dat er toegang is tot de /var/www/html/ file is. Dit is de locatie waar de deze programma's gebruikt zullen worden voor het lezen van de applicatie webpagina.
Er word ook een database aangemaakt in bookstack.

## send.sh
Dit document word gebruikt voor meldingen door te sturen over de status van de pipeline naar onze discord pagina. Er worden verschillende mogelijke situaties ingeprogrameerd: succes, failure en status unknown. Als de status onbekend is betekend dit dat de gebruiker nog de "author_name", "comitter_name", "commit_subject" en "commit_message" moet toevoegen. <br />

De discord notificatie bevat volgende onderdelen: 
- Het gitlab logo
- De status kleur (succes = groen, failed = rood, gitlab acties= orange )
- Pipeline id met status bericht + project locatie
- Titel
- Beschrijving
- Commiter_name + actie
- Commit referentie naam
- Branch naam
- datum  + tijdstip


## Hoe gebruik maken van onze oplossing
Voor de pipeline kan gebruikt worden is het belangrijk de credentials worden aangepast. Dit gebeurt via: 'settings' > 'CI/CD' > 'Variabeles' volgende variabelen te definiëren:
- GITLAB_TOKEN
- SSH_PRIVATE_KEY
- TF_VAR_aws_acces_key
- TF_VAR_aws_secret_key
- TF_VAR_aws_token
- TF_VAR_db_password
- TF_VAR_db_username
- TF_VAR_my_ip
- WEBHOOK_URL
<br />

Vervolgens is het belangrijk de pipeline door al de stages te doen lopen. Wanneer de pipeline is afgerond en de notificatie van de laatse stap succesvol is, kan de toepassing worden gebruikt.
